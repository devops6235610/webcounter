# Webcounter
Simple Python Webcounter with redis server
## Build
docker build -t jmmprates/webcounter:1.0.0 .
## Dependencies
docker run -d  --name redis --rm redis:alpine
## Run
docker run -d --rm -p80:5000 --name webcounter --link redis -e REDIS_URL=redis jmmprates/webcounter:1.0.0
### Gitlab register
gitlab-runner register -n \
--url https://gitlab.com/ \
--executor shell \
--description "docker-lab" \
--tag-list "production" \
--registration-token GR1348941ea2xeunQ258es-jwFxzF